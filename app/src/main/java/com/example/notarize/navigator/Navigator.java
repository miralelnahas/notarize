package com.example.notarize.navigator;

import android.app.Activity;
import android.content.Intent;

import com.example.notarize.model.Entity.transaction.TransactionResponse;
import com.example.notarize.view.activities.CreateTransactionActivity;
import com.example.notarize.view.activities.UpdateTransactionActivity;
import com.example.notarize.view.activities.showTransaction.ShowTransactionActivity;

public final class Navigator {

    private Navigator() {
    }

    public static void startCreateTransactionActivity(Activity activity) {
        Intent intent = new Intent(activity, CreateTransactionActivity.class);
        activity.startActivityForResult(intent, 1);
    }

    public static void startTransactionActivity(Activity activity, String transactionId) {
        Intent intent = new Intent(activity, ShowTransactionActivity.class);
        intent.putExtra("transactionId", transactionId);
        activity.startActivityForResult(intent, 7);
    }

    public static void startUpdateTransactionActivity(Activity activity, TransactionResponse transactionResponse) {
        Intent intent = new Intent(activity, UpdateTransactionActivity.class);
        intent.putExtra("transactionResponse", transactionResponse);
        activity.startActivityForResult(intent, 2);
    }
}
