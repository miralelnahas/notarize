package com.example.notarize.view.activities.listTransactions;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.notarize.R;
import com.example.notarize.callbacks.TransactionsOnClickListener;
import com.example.notarize.model.Entity.transaction.Signer;
import com.example.notarize.model.Entity.transaction.TransactionResponse;
import com.example.notarize.navigator.Navigator;

import java.util.ArrayList;

public class ListTransactionsAdapter extends RecyclerView.Adapter<ListTransactionsAdapter.ViewHolder> {


    private Activity activity;
    private ArrayList<TransactionResponse> transactions;
    private TransactionsOnClickListener transactionsOnClickListener;

    ListTransactionsAdapter(Activity activity, ArrayList<TransactionResponse> transactions,
                            TransactionsOnClickListener transactionsOnClickListener) {
        this.transactions = transactions;
        this.activity = activity;
        this.transactionsOnClickListener = transactionsOnClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_transactions_row, parent, false);
        return new ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Signer signer = transactions.get(position).getSigners()[0];
        String id = transactions.get(position).getId();
        holder.signerName.setText(signer.getFirstName() + " " + signer.getLastName());
        if (transactions.get(position).getDocuments().length > 0) {
            holder.documentName.setText(transactions.get(position).getDocuments()[0].getDocumentName());
        }

        if (transactions.get(position).getStatus().equals("started")) {
            holder.activate.setVisibility(View.VISIBLE);
            holder.transactionCard.setCardBackgroundColor(Color.parseColor("#fffcd7"));

        } else {
            holder.resend.setVisibility(View.VISIBLE);
            holder.transactionCard.setCardBackgroundColor(Color.parseColor("#d8fff5"));
        }
        holder.itemView.setOnClickListener(view -> {
            Navigator.startTransactionActivity(activity, transactions.get(position).getId());
        });
        holder.delete.setOnClickListener(view -> transactionsOnClickListener.onDeleteClick(id));
        holder.resend.setOnClickListener(view -> transactionsOnClickListener.onResendClick(id));
        holder.activate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transactionsOnClickListener.onActivateClick(id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView signerName;
        TextView documentName;
        ImageView resend;
        ImageView delete;
        ImageView activate;
        CardView transactionCard;


        ViewHolder(@NonNull View itemView) {
            super(itemView);
            signerName = itemView.findViewById(R.id.doc);
            documentName = itemView.findViewById(R.id.documentName);
            resend = itemView.findViewById(R.id.resend);
            delete = itemView.findViewById(R.id.delete);
            activate = itemView.findViewById(R.id.activate);
            transactionCard = itemView.findViewById(R.id.transactionCard);
        }
    }
}
