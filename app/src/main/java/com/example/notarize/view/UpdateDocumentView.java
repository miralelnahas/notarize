package com.example.notarize.view;

public interface UpdateDocumentView extends BaseView {
    void sendUpdateDocumentResponse();
}
