package com.example.notarize.view.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.notarize.R;
import com.example.notarize.model.Entity.transaction.Signer;
import com.example.notarize.model.Entity.transaction.TransactionResponse;
import com.example.notarize.model.Entity.transaction.UpdateTransactionData;
import com.example.notarize.model.inject.component.DaggerUpdateTransactionComponent;
import com.example.notarize.model.inject.component.UpdateTransactionComponent;
import com.example.notarize.presenter.UpdateTransactionPresenterContract;
import com.example.notarize.view.UpdateTransactionView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UpdateTransactionActivity
        extends BaseActivity<UpdateTransactionPresenterContract, UpdateTransactionView, UpdateTransactionComponent>
        implements UpdateTransactionView {

    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.firstName)
    EditText firstName;
    @BindView(R.id.middleName)
    EditText middleName;
    @BindView(R.id.lastName)
    EditText lastName;
    String id;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected int getResId() {
        return R.layout.activity_update_transaction;
    }

    @Override
    protected void inject() {
        getComponent().inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_transaction);
        setTitle("Update Transaction");
        ButterKnife.bind(this);
        showUserData();

    }

    void showUserData() {
        TransactionResponse transactionResponse = getIntent().getParcelableExtra("transactionResponse");
        id = transactionResponse.getId();
        Signer signer = transactionResponse.getSigners()[0];
        email.setText(signer.getEmail());
        firstName.setText(signer.getFirstName());
        middleName.setText(signer.getMiddleName());
        lastName.setText(signer.getLastName());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    public void updateTransaction(View view) {
        Signer signer = new Signer(email.getText().toString(),
                firstName.getText().toString(), middleName.getText().toString(), lastName.getText().toString());
        UpdateTransactionData updateTransactionData = new UpdateTransactionData(signer);
        showProgressBar(progressBar);
        getPresenter().updateTransaction(id, updateTransactionData);
    }

    @Override
    public UpdateTransactionComponent getComponent() {
        return DaggerUpdateTransactionComponent.builder()
                .applicationComponent(getApplicationComponent()).build();
    }

    @Override
    public void sendUpdateTransactionResponse() {
        hideProgressBar(progressBar);
        setResult(RESULT_OK, getIntent());
        finish();
    }

    @Override
    public void sendUpdateTransactionError() {
        Toast.makeText(this, "Something went wrong.", Toast.LENGTH_SHORT).show();
        hideProgressBar(progressBar);
    }
}
