package com.example.notarize.view;

import com.example.notarize.model.Entity.transaction.DeleteDocumentResponse;
import com.example.notarize.model.Entity.transaction.TransactionResponse;

public interface TransactionView extends BaseView {
    void sendGetTransactionResponse(TransactionResponse transactionResponse);

    void sendGetTransactionError();

    void sendDeleteDocumentResponse(DeleteDocumentResponse value);

    void sendAddDocumentResponse();

    void sendDeleteTransactionResponse(String response);

    void sendResendEmailResponse();

    void sendActivateTransactionResponse();

    void sendError(String error);
}
