package com.example.notarize.view;

public interface UpdateTransactionView extends BaseView {
    void sendUpdateTransactionResponse();
    void sendUpdateTransactionError();
}
