package com.example.notarize.view.activities;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.notarize.R;
import com.example.notarize.model.Entity.transaction.Signer;
import com.example.notarize.model.Entity.transaction.TransactionData;
import com.example.notarize.model.Entity.transaction.TransactionResponse;
import com.example.notarize.model.inject.component.CreateTransactionComponent;
import com.example.notarize.model.inject.component.DaggerCreateTransactionComponent;
import com.example.notarize.presenter.CreateTransactionPresenterContract;
import com.example.notarize.view.CreateTransactionView;

import java.io.File;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateTransactionActivity
        extends BaseActivity<CreateTransactionPresenterContract, CreateTransactionView, CreateTransactionComponent>
        implements CreateTransactionView {

    private static final String TAG = "data";
    @BindView(R.id.documentUrl)
    EditText documentUrl;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.firstName)
    EditText firstName;
    @BindView(R.id.middleName)
    EditText middleName;
    @BindView(R.id.lastName)
    EditText lastName;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.draftSwitch)
    Switch draftSwitch;
    private static final int REQUEST_WRITE_STORAGE = 112;
    private static final int REQUEST_READ_STORAGE = 111;
    String path;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResId());
        setTitle("Create Transaction");
        ButterKnife.bind(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    public void addTransaction(View view) {
        if (validAddTransaction()) {
            String draft = "false";
            if (draftSwitch.isChecked()) {
                draft = "true";
            }
            Signer signer = new Signer(
                    String.valueOf(email.getText()),
                    String.valueOf(firstName.getText()),
                    String.valueOf(middleName.getText()),
                    String.valueOf(lastName.getText())
            );
            TransactionData data = new TransactionData(
                    String.valueOf(documentUrl.getText()),
                    signer, draft
            );
            getPresenter().addTransaction(data);
            showProgressBar(progressBar);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void selectFile(View view) {
        boolean hasPermission = (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(CreateTransactionActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_WRITE_STORAGE);
        }

        boolean hasReadPermission = (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasReadPermission) {
            ActivityCompat.requestPermissions(CreateTransactionActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_READ_STORAGE);
        } else {
            selectPdfFile();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_WRITE_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectPdfFile();
            } else {
                Toast.makeText(this, "You must give access to storage.", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == REQUEST_READ_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectPdfFile();
            } else {
                Toast.makeText(this, "You must give access to storage.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void selectPdfFile() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("application/pdf");
        Intent pdf = Intent.createChooser(intent, "Select PDF file");
        startActivityForResult(pdf, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            Uri selectedPdfUri = data.getData();
            File file = new File(selectedPdfUri.getPath());
            final String[] split = file.getPath().split(":");
            String filePath = split[1];
            filePath = Environment.getExternalStorageDirectory() + "/" + filePath;
            file = new File(filePath);
            documentUrl.setText(filePath);
            if (file.exists()) {
                Log.i("length", String.valueOf(file.length()));
                Intent pdfOpenintent = new Intent(Intent.ACTION_VIEW);
                Intent chooser = Intent.createChooser(pdfOpenintent, "Select App");
                pdfOpenintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                pdfOpenintent.setDataAndType(selectedPdfUri, "application/pdf");
                try {
                    startActivity(chooser);
                } catch (ActivityNotFoundException e) {
                    Log.e("Exception", Objects.requireNonNull(e.getMessage()));
                }
            } else {
                Toast.makeText(this, "File Doesn't exist", Toast.LENGTH_SHORT).show();
            }
        }
    }


    Boolean validAddTransaction() {
        if (isEmpty(documentUrl) || isEmpty(email) || isEmpty(firstName) || isEmpty(middleName) || isEmpty(lastName)) {
            return false;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email.getText()).matches()) {
            email.setError("Email Format Required");
            email.requestFocus();
            return false;
        }

        return true;
    }

    Boolean isEmpty(EditText editText) {
        if (TextUtils.isEmpty(editText.getText())) {
            editText.setError("Field Required");
            editText.requestFocus();
            return true;
        }
        return false;
    }

    @Override
    protected int getResId() {
        return R.layout.activity_create_transaction;
    }

    @Override
    protected void inject() {
        getComponent().inject(this);
    }

    @Override
    public CreateTransactionComponent getComponent() {
        return DaggerCreateTransactionComponent.builder()
                .applicationComponent(getApplicationComponent()).build();
    }

    @Override
    public void sendAddTransactionResponse(TransactionResponse transactionResponse) {
        hideProgressBar(progressBar);

        Toast.makeText(getApplicationContext(), "Transaction Added Successfully", Toast.LENGTH_SHORT).show();

        Intent intent = getIntent();
        intent.putExtra("Transaction ID", transactionResponse.getId());
        setResult(RESULT_OK, intent);
        finish();
    }
}
