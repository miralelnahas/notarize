package com.example.notarize.view.activities.showTransaction;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.notarize.R;
import com.example.notarize.callbacks.DocumentsOnClickListener;
import com.example.notarize.model.Entity.transaction.AddDocumentData;
import com.example.notarize.model.Entity.transaction.DeleteDocumentResponse;
import com.example.notarize.model.Entity.transaction.TransactionResponse;
import com.example.notarize.model.inject.component.DaggerTransactionComponent;
import com.example.notarize.model.inject.component.TransactionComponent;
import com.example.notarize.navigator.Navigator;
import com.example.notarize.presenter.TransactionPresenterContract;
import com.example.notarize.view.TransactionView;
import com.example.notarize.view.activities.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShowTransactionActivity
        extends BaseActivity<TransactionPresenterContract, TransactionView, TransactionComponent>
        implements TransactionView, DocumentsOnClickListener {

    String id;
    @BindView(R.id.progressBar6)
    ProgressBar progressBar;
    @BindView(R.id.transactionId)
    TextView transactionId;
    @BindView(R.id.status)
    TextView statusView;
    @BindView(R.id.documentsRecyclerView)
    RecyclerView documentsRecyclerView;
    @BindView(R.id.firstName)
    TextView firstName;
    @BindView(R.id.middleName)
    TextView middleName;
    @BindView(R.id.lastName)
    TextView lastName;
    @BindView(R.id.deleteButton)
    ImageView deleteButton;
    @BindView(R.id.activateButton)
    ImageView activateButton;
    @BindView(R.id.resendButton)
    ImageView resendButton;
    @BindView(R.id.editButton)
    ImageView editButton;
    @BindView(R.id.addDocument)
    CardView addDocumentButton;
    TransactionResponse transactionResponse;

    @Override
    protected int getResId() {
        return R.layout.activity_show_transaction;
    }

    @Override
    protected void inject() {
        getComponent().inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResId());
        ButterKnife.bind(this);
        setTitle("Transaction Details");
        requestTransaction();
    }

    void requestTransaction() {
        Intent intent = getIntent();
        id = intent.getStringExtra("transactionId");
        showProgressBar(progressBar);
        getPresenter().getTransaction(id);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void sendGetTransactionResponse(TransactionResponse transactionResponse) {
        this.transactionResponse = transactionResponse;
        hideProgressBar(progressBar);
        transactionId.setText(transactionResponse.getId());
        if (transactionResponse.getDocuments().length > 0) {
            setAdapter(transactionResponse.getDocuments().length);
        } else {
            addDocumentButton.setVisibility(View.VISIBLE);
        }
        firstName.setText(transactionResponse.getSigners()[0].getFirstName());
        middleName.setText(transactionResponse.getSigners()[0].getMiddleName());
        lastName.setText(transactionResponse.getSigners()[0].getLastName());
        if (transactionResponse.getStatus().equals("started")) {
            statusView.setText("Draft");
            resendButton.setVisibility(View.GONE);
            deleteButton.setVisibility(View.VISIBLE);
            activateButton.setVisibility(View.VISIBLE);
            editButton.setVisibility(View.VISIBLE);
        } else {
            statusView.setText("Active (" + transactionResponse.getStatus() + ")");
            deleteButton.setVisibility(View.VISIBLE);
            activateButton.setVisibility(View.GONE);
            editButton.setVisibility(View.GONE);
            resendButton.setVisibility(View.VISIBLE);
        }
    }

    void setAdapter(int count) {
        documentsRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        documentsRecyclerView.setAdapter(new DocumentsAdapter(transactionResponse.getDocuments(),
                transactionResponse.getStatus(), this,
                count));
    }

    @Override
    public void sendGetTransactionError() {
        showToast("Transaction not found");
        hideProgressBar(progressBar);
        finish();
    }

    @Override
    public void sendDeleteDocumentResponse(DeleteDocumentResponse value) {
        showToast(value.getMessage());
        if (transactionResponse.getDocuments().length == 1) {
            setAdapter(0);
        }
        getPresenter().getTransaction(id);
    }

    @Override
    public void sendAddDocumentResponse() {
        showToast("Document Added Successfully");
        getPresenter().getTransaction(id);
    }

    @Override
    public void sendError(String error) {
        showToast(error);
        hideProgressBar(progressBar);
    }

    @Override
    public TransactionComponent getComponent() {
        return DaggerTransactionComponent.builder()
                .applicationComponent(getApplicationComponent()).build();
    }

    @Override
    public void onDeleteClick(String id) {
        showProgressBar(progressBar);
        getPresenter().deleteDocument(id);
    }

    @Override
    public void onSubmitClick(String documentUrl) {
        showProgressBar(progressBar);
        getPresenter().addDocument(id, new AddDocumentData(documentUrl));
    }

    @Override
    public void onCancelClick() {
        addDocumentButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void sendResendEmailResponse() {
        hideProgressBar(progressBar);
        showToast("Email resent successfully");
    }


    @Override
    public void sendDeleteTransactionResponse(String response) {
        Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
        finish();
    }


    @Override
    public void sendActivateTransactionResponse() {
        showToast("Transaction Activated Successfully");
        getPresenter().getTransaction(id);
    }


    public void activateTransaction(View view) {
        showProgressBar(progressBar);
        getPresenter().activateTransaction(id);
    }

    public void resendEmail(View view) {
        showProgressBar(progressBar);
        getPresenter().resendEmail(id);
    }

    public void deleteTransaction(View view) {
        showProgressBar(progressBar);
        getPresenter().deleteTransaction(id);
    }

    public void editTransaction(View view) {
        Navigator.startUpdateTransactionActivity(this, transactionResponse);
    }

    public void addDocument(View view) {
        setAdapter(transactionResponse.getDocuments().length);
        addDocumentButton.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2 && resultCode == RESULT_OK) {
            showProgressBar(progressBar);
            getPresenter().getTransaction(id);
        }
    }
}
