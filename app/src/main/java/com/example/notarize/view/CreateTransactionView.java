package com.example.notarize.view;

import com.example.notarize.model.Entity.transaction.TransactionResponse;

public interface CreateTransactionView extends BaseView {
    void sendAddTransactionResponse(TransactionResponse transactionResponse);
}
