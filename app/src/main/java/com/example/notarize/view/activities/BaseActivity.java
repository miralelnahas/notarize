package com.example.notarize.view.activities;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.notarize.NotarizeApplication;
import com.example.notarize.model.inject.HasComponent;
import com.example.notarize.model.inject.component.ApplicationComponent;
import com.example.notarize.presenter.BasePresenter;
import com.example.notarize.view.BaseView;

import javax.inject.Inject;

public abstract class BaseActivity<P extends BasePresenter, V extends BaseView, C>
        extends AppCompatActivity implements BaseView, HasComponent<C> {

    @Inject
    P presenter;

    protected abstract int getResId();

    protected abstract void inject();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inject();
        presenter.onCreate();
        presenter.attachView(this);
    }

    protected ApplicationComponent getApplicationComponent() {
        return NotarizeApplication.getApplicationComponent();
    }

    public P getPresenter() {
        return presenter;
    }

    @Override
    protected void onStart() {
        presenter.onStart();
        super.onStart();
    }

    @Override
    protected void onResume() {
        presenter.onResume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        presenter.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        presenter.onStop();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    public void showProgressBar(ProgressBar progressBar) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar(ProgressBar progressBar) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
