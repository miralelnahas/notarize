package com.example.notarize.view.activities.showTransaction;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.notarize.R;
import com.example.notarize.callbacks.DocumentsOnClickListener;
import com.example.notarize.model.Entity.transaction.Document;

public class DocumentsAdapter extends RecyclerView.Adapter<DocumentsAdapter.BaseViewHolder> {

    private Document[] documents;
    private DocumentsOnClickListener documentsOnClickListener;
    private String status;
    private int count;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;

    DocumentsAdapter(Document[] document, String status, DocumentsOnClickListener documentsOnClickListener, int count) {
        this.documents = document;
        this.documentsOnClickListener = documentsOnClickListener;
        this.status = status;
        this.count = count;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        }

        return TYPE_ITEM;
    }

    private boolean isPositionFooter(int position) {
        return position == documents.length;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.documents_row, parent, false);
            return new DocumentViewHolder(v);
        } else if (viewType == TYPE_FOOTER) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.documents_footer, parent, false);
            return new DocumentsAdapter.FooterViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        if (holder instanceof DocumentViewHolder) {
            ((DocumentViewHolder) holder).documentName.setText(documents[position].getDocumentName());

            ((DocumentViewHolder) holder).delete.setOnClickListener(view -> {
                documentsOnClickListener.onDeleteClick(documents[position].getId());
            });
            if (status.equals("started")) {
                ((DocumentViewHolder) holder).delete.setVisibility(View.VISIBLE);
                if (position == count - 1) {
                    ((DocumentViewHolder) holder).addDocument.setVisibility(View.VISIBLE);
                    ((DocumentViewHolder) holder).addDocument.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ((DocumentViewHolder) holder).addDocument.setVisibility(View.INVISIBLE);
                            count += 1;
                            notifyDataSetChanged();
                        }
                    });
                }
            } else {
                ((DocumentViewHolder) holder).addDocument.setVisibility(View.GONE);
            }
        } else if (holder instanceof FooterViewHolder) {
            ((FooterViewHolder) holder).cancel.setOnClickListener(view -> {
                notifyDataSetChanged();
                count -= 1;
                if (documents.length == 0) {
                    documentsOnClickListener.onCancelClick();
                }

            });
            ((FooterViewHolder) holder).submit.setOnClickListener(view -> {
                EditText documentUrl = ((FooterViewHolder) holder).documentUrl;
                if (TextUtils.isEmpty(documentUrl.getText())) {
                    documentUrl.setError("Field Required");
                } else {
                    documentsOnClickListener.onSubmitClick(documentUrl.getText().toString());
                }

            });
        }
    }

    @Override
    public int getItemCount() {
        if (count == -1) {
            return 0;
        }
        if (documents.length == 0) {
            return 1;
        }
        return count;
    }

    class BaseViewHolder extends RecyclerView.ViewHolder {

        BaseViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    class DocumentViewHolder extends BaseViewHolder {

        TextView documentName;
        ImageView delete;
        CardView addDocument;

        DocumentViewHolder(@NonNull View itemView) {
            super(itemView);
            documentName = itemView.findViewById(R.id.documentName);
            delete = itemView.findViewById(R.id.delete);
            addDocument = itemView.findViewById(R.id.addDocument);
        }
    }

    class FooterViewHolder extends BaseViewHolder {
        ImageView submit;
        ImageView cancel;
        EditText documentUrl;

        FooterViewHolder(@NonNull View itemView) {
            super(itemView);
            submit = itemView.findViewById(R.id.submit);
            cancel = itemView.findViewById(R.id.cancel);
            documentUrl = itemView.findViewById(R.id.documentUrl);
        }
    }
}
