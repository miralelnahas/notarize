package com.example.notarize.view;

import com.example.notarize.model.Entity.transaction.TransactionResponse;

import java.util.ArrayList;

public interface ListTransactionsView extends BaseView {
    void showTransactions(ArrayList<TransactionResponse> transactions);

    void sendResponse(String response);

    void sendError(String error);

    void sendResendEmailResponse();
}
