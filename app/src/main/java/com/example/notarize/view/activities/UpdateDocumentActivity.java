package com.example.notarize.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.notarize.R;
import com.example.notarize.model.Entity.transaction.Document;
import com.example.notarize.model.Entity.transaction.UpdateDocumentData;
import com.example.notarize.model.inject.component.DaggerUpdateDocumentComponent;
import com.example.notarize.model.inject.component.UpdateDocumentComponent;
import com.example.notarize.presenter.UpdateDocumentPresenterContract;
import com.example.notarize.view.UpdateDocumentView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UpdateDocumentActivity
        extends BaseActivity<UpdateDocumentPresenterContract, UpdateDocumentView, UpdateDocumentComponent>
        implements UpdateDocumentView {
    @BindView(R.id.documentName)
    EditText documentName;
    @BindView(R.id.progressBar3)
    ProgressBar progressBar;

    Document document;

    @Override
    protected int getResId() {
        return R.layout.activity_update_document;
    }

    @Override
    protected void inject() {
        getComponent().inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_document);
        ButterKnife.bind(this);
        Intent intent = getIntent();

        document = intent.getParcelableExtra("document");
        documentName.setText(document != null ? document.getDocumentName() : null);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    public void updateDocument(View view) {
        UpdateDocumentData updateDocumentData = new UpdateDocumentData(documentName.getText().toString());
        getPresenter().updateDocument(document.getId(), updateDocumentData);
        progressBar.setVisibility(View.VISIBLE);

        //To disable user interaction
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public UpdateDocumentComponent getComponent() {
        return DaggerUpdateDocumentComponent.builder()
                .applicationComponent(getApplicationComponent()).build();
    }

    @Override
    public void sendUpdateDocumentResponse() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.INVISIBLE);
        setResult(RESULT_OK, getIntent());
        finish();
    }
}
