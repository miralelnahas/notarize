package com.example.notarize.view.activities.listTransactions;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.notarize.R;
import com.example.notarize.callbacks.TransactionsOnClickListener;
import com.example.notarize.model.Entity.transaction.TransactionResponse;
import com.example.notarize.model.inject.component.DaggerListTransactionsComponent;
import com.example.notarize.model.inject.component.ListTransactionsComponent;
import com.example.notarize.navigator.Navigator;
import com.example.notarize.presenter.ListTransactionsPresenterContract;
import com.example.notarize.view.ListTransactionsView;
import com.example.notarize.view.activities.BaseActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListTransactionsActivity
        extends BaseActivity<ListTransactionsPresenterContract, ListTransactionsView, ListTransactionsComponent>
        implements ListTransactionsView, TransactionsOnClickListener {
    @BindView(R.id.transactionsRecyclerView)
    RecyclerView transactionsRecyclerView;
    @BindView(R.id.progressBar4)
    ProgressBar progressBar;
    @BindView(R.id.createFloatingButton)
    View createFloatingButton;
    String id;

    @Override
    protected int getResId() {
        return R.layout.activity_list_transactions;
    }

    @Override
    protected void inject() {
        getComponent().inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResId());
        ButterKnife.bind(this);
        showProgressBar(progressBar);
        getPresenter().listTransactions();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        showProgressBar(progressBar);
        getPresenter().listTransactions();
    }

    @Override
    public ListTransactionsComponent getComponent() {
        return DaggerListTransactionsComponent.builder()
                .applicationComponent(getApplicationComponent()).build();
    }

    public void showTransactions(ArrayList<TransactionResponse> transactions) {
        transactionsRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        transactionsRecyclerView.setAdapter(new ListTransactionsAdapter(this, transactions, this));
        hideProgressBar(progressBar);
        createFloatingButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void sendResponse(String response) {
        Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
        getPresenter().listTransactions();
    }

    @Override
    public void sendError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
        hideProgressBar(progressBar);
    }

    @Override
    public void sendResendEmailResponse() {
        hideProgressBar(progressBar);
        Toast.makeText(this, "Email resent successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeleteClick(String id) {
        showProgressBar(progressBar);
        getPresenter().deleteTransaction(id);
    }

    @Override
    public void onResendClick(String id) {
        showProgressBar(progressBar);
        getPresenter().resendEmail(id);
    }

    @Override
    public void onActivateClick(String id) {
        showProgressBar(progressBar);
        getPresenter().activateTransaction(id);
    }

    public void createTransaction(View view) {
        Navigator.startCreateTransactionActivity(this);
    }

}
