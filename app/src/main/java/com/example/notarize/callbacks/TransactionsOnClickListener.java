package com.example.notarize.callbacks;

public interface TransactionsOnClickListener {
    void onDeleteClick(String id);
    void onResendClick(String id);
    void onActivateClick(String id);
}
