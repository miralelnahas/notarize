package com.example.notarize.callbacks;

public interface DocumentsOnClickListener {
    void onDeleteClick(String id);
    void onSubmitClick(String documentUrl);
    void onCancelClick();
}
