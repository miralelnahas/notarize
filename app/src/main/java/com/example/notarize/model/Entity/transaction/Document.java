package com.example.notarize.model.Entity.transaction;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Document implements Parcelable {
    @SerializedName("document_name")
    @Expose
    String documentName;

    @SerializedName("date_created")
    @Expose
    String dateCreated;

    @SerializedName("date_updated")
    @Expose
    String dateUpdated;

    @SerializedName("id")
    @Expose
    String id;

    protected Document(Parcel in) {
        documentName = in.readString();
        dateCreated = in.readString();
        dateUpdated = in.readString();
        id = in.readString();
    }

    public static final Creator<Document> CREATOR = new Creator<Document>() {
        @Override
        public Document createFromParcel(Parcel in) {
            return new Document(in);
        }

        @Override
        public Document[] newArray(int size) {
            return new Document[size];
        }
    };

    public String getDocumentName() {
        return documentName;
    }
    public String getDateCreated() {
        return dateCreated;
    }

    public String getDateUpdated() {
        return dateUpdated;
    }

    public String getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(documentName);
        parcel.writeString(dateCreated);
        parcel.writeString(dateUpdated);
        parcel.writeString(id);
    }
}
