package com.example.notarize.model.Entity.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateDocumentData {
    @SerializedName("name")
    @Expose
    String name;

    public UpdateDocumentData(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
