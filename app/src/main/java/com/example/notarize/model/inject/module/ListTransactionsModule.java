package com.example.notarize.model.inject.module;

import com.example.notarize.model.interactor.ListTransactionsInteractorContract;
import com.example.notarize.presenter.ListTransactionsPresenter;
import com.example.notarize.presenter.ListTransactionsPresenterContract;

import dagger.Module;
import dagger.Provides;

@Module
public class ListTransactionsModule {
    @Provides
    ListTransactionsPresenterContract providesListTransactionsPresenter(
            ListTransactionsInteractorContract listTransactionsInteractor) {
        return new ListTransactionsPresenter(listTransactionsInteractor);
    }
}
