package com.example.notarize.model.inject.module;

import com.example.notarize.model.interactor.CreateTransactionInteractor;
import com.example.notarize.model.interactor.CreateTransactionInteractorContract;
import com.example.notarize.model.interactor.ListTransactionsInteractor;
import com.example.notarize.model.interactor.ListTransactionsInteractorContract;
import com.example.notarize.model.interactor.TransactionInteractor;
import com.example.notarize.model.interactor.TransactionInteractorContract;
import com.example.notarize.model.interactor.UpdateDocumentInteractor;
import com.example.notarize.model.interactor.UpdateDocumentInteractorContract;
import com.example.notarize.model.interactor.UpdateTransactionInteractor;
import com.example.notarize.model.interactor.UpdateTransactionInteractorContract;

import dagger.Module;
import dagger.Provides;

@Module
public class InteractorModule {
    @Provides
    CreateTransactionInteractorContract providesCreateTransactionInteractor() {
        return new CreateTransactionInteractor();
    }
    @Provides
    UpdateTransactionInteractorContract providesUpdateTransactionInteractor() {
        return new UpdateTransactionInteractor();
    }
    @Provides
    TransactionInteractorContract providesTransactionInteractor() {
        return new TransactionInteractor();
    }
    @Provides
    UpdateDocumentInteractorContract providesUpdateDocumentInteractor() {
        return new UpdateDocumentInteractor();
    }
    @Provides
    ListTransactionsInteractorContract providesListTransactionsInteractor() {
        return new ListTransactionsInteractor();
    }

}
