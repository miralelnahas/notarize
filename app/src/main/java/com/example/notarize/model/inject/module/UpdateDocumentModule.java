package com.example.notarize.model.inject.module;

import com.example.notarize.model.interactor.UpdateDocumentInteractorContract;
import com.example.notarize.presenter.UpdateDocumentPresenterContract;
import com.example.notarize.presenter.UpdateDocumentPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class UpdateDocumentModule {
    @Provides
    UpdateDocumentPresenterContract providesUpdateDocumentPresenter(
            UpdateDocumentInteractorContract updateDocumentInteractor) {
        return new UpdateDocumentPresenter(updateDocumentInteractor);
    }

}
