package com.example.notarize.model.inject.module;

import com.example.notarize.model.interactor.TransactionInteractorContract;
import com.example.notarize.presenter.TransactionPresenter;
import com.example.notarize.presenter.TransactionPresenterContract;

import dagger.Module;
import dagger.Provides;

@Module
public class TransactionModule {
    @Provides
    TransactionPresenterContract providesTransactionPresenter(TransactionInteractorContract transactionInteractor) {
        return new TransactionPresenter(transactionInteractor);
    }
}
