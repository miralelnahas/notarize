package com.example.notarize.model.interactor;

import com.example.notarize.model.Entity.transaction.AddDocumentData;
import com.example.notarize.model.Entity.transaction.DeleteDocumentResponse;
import com.example.notarize.model.Entity.transaction.DeleteTransactionResponse;
import com.example.notarize.model.Entity.transaction.Document;
import com.example.notarize.model.Entity.transaction.TransactionData;
import com.example.notarize.model.Entity.transaction.TransactionResponse;
import com.example.notarize.model.Entity.transaction.Transactions;
import com.example.notarize.model.Entity.transaction.UpdateDocumentData;
import com.example.notarize.model.Entity.transaction.UpdateTransactionData;
import com.example.notarize.model.api.NotarizeService;
import com.example.notarize.model.network.NetworkClient;

import io.reactivex.Observable;

public abstract class BaseInteractor {
    Observable<TransactionResponse> addTransaction(TransactionData data) {
        NotarizeService service = NetworkClient.getNetworkInstance().getRetrofitClient().create(NotarizeService.class);
        return service.addTransaction(data);
    }

    Observable<TransactionResponse> getTransaction(String id) {
        NotarizeService service = NetworkClient.getNetworkInstance().getRetrofitClient().create(NotarizeService.class);
        return service.getTransaction(id);
    }

    public Observable<DeleteTransactionResponse> deleteTransaction(String id) {
        NotarizeService service = NetworkClient.getNetworkInstance().getRetrofitClient().create(NotarizeService.class);
        return service.deleteTransaction(id);
    }

    Observable<TransactionResponse> updateTransaction(String id, UpdateTransactionData updateTransactionData) {
        NotarizeService service = NetworkClient.getNetworkInstance().getRetrofitClient().create(NotarizeService.class);
        return service.updateTransaction(id, updateTransactionData);
    }

    Observable<Document> addDocument(String id, AddDocumentData addDocumentData) {
        NotarizeService service = NetworkClient.getNetworkInstance().getRetrofitClient().create(NotarizeService.class);
        return service.addDocument(id, addDocumentData);
    }

    public Observable<TransactionData> activateTransaction(String id) {
        NotarizeService service = NetworkClient.getNetworkInstance().getRetrofitClient().create(NotarizeService.class);
        return service.activateTransaction(id);
    }

    Observable<DeleteDocumentResponse> deleteDocument(String id) {
        NotarizeService service = NetworkClient.getNetworkInstance().getRetrofitClient().create(NotarizeService.class);
        return service.deleteDocument(id);
    }

    Observable<Document> updateDocument(String id, UpdateDocumentData updateDocumentData) {
        NotarizeService service = NetworkClient.getNetworkInstance().getRetrofitClient().create(NotarizeService.class);
        return service.updateDocument(id, updateDocumentData);
    }

    Observable<Transactions> listTransactions() {
        NotarizeService service = NetworkClient.getNetworkInstance().getRetrofitClient().create(NotarizeService.class);
        return service.listTransactions();
    }

    public Observable<TransactionResponse> resendEmail(String id) {
        NotarizeService service = NetworkClient.getNetworkInstance().getRetrofitClient().create(NotarizeService.class);
        return service.resendEmail(id);
    }
}
