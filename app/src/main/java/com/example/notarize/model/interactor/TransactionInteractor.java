package com.example.notarize.model.interactor;

import com.example.notarize.model.Entity.transaction.AddDocumentData;
import com.example.notarize.model.Entity.transaction.DeleteDocumentResponse;
import com.example.notarize.model.Entity.transaction.DeleteTransactionResponse;
import com.example.notarize.model.Entity.transaction.Document;
import com.example.notarize.model.Entity.transaction.TransactionData;
import com.example.notarize.model.Entity.transaction.TransactionResponse;

import io.reactivex.Observable;

public class TransactionInteractor extends TransactionInteractorContract {
    @Override
    public Observable<TransactionResponse> getTransaction(String id) {
        return super.getTransaction(id);
    }


    @Override
    public Observable<DeleteTransactionResponse> deleteTransaction(String id) {
        return super.deleteTransaction(id);
    }

    @Override
    public Observable<TransactionResponse> resendEmail(String id) {
        return super.resendEmail(id);
    }

    @Override
    public Observable<TransactionData> activateTransaction(String id) {
        return super.activateTransaction(id);
    }

    @Override
    public Observable<DeleteDocumentResponse> deleteDocument(String id) {
        return super.deleteDocument(id);
    }

    @Override
    public Observable<Document> addDocument(String id, AddDocumentData addDocumentData) {
        return super.addDocument(id, addDocumentData);
    }
}
