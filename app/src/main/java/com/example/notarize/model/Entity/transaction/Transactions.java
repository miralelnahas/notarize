package com.example.notarize.model.Entity.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Transactions {
    @SerializedName("count")
    @Expose
    int count;

    @SerializedName("data")
    @Expose
    ArrayList<TransactionResponse> transactions;

    public Transactions(int count, ArrayList<TransactionResponse> transactionResponses) {
        this.count = count;
        this.transactions = transactionResponses;
    }

    public int getCount() {
        return count;
    }

    public ArrayList<TransactionResponse> getTransactions() {
        return transactions;
    }
}
