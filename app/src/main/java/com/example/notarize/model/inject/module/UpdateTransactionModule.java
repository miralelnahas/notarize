package com.example.notarize.model.inject.module;

import com.example.notarize.model.interactor.UpdateTransactionInteractorContract;
import com.example.notarize.presenter.UpdateTransactionPresenter;
import com.example.notarize.presenter.UpdateTransactionPresenterContract;

import dagger.Module;
import dagger.Provides;

@Module
public class UpdateTransactionModule {
    @Provides
    UpdateTransactionPresenterContract providesUpdateTransactionPresenter(
            UpdateTransactionInteractorContract updateTransactionInteractor) {
        return new UpdateTransactionPresenter(updateTransactionInteractor);
    }
}
