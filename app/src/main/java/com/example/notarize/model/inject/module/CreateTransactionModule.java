package com.example.notarize.model.inject.module;

import com.example.notarize.model.interactor.CreateTransactionInteractorContract;
import com.example.notarize.presenter.CreateTransactionPresenter;
import com.example.notarize.presenter.CreateTransactionPresenterContract;

import dagger.Module;
import dagger.Provides;

@Module
public class CreateTransactionModule {
    @Provides
    CreateTransactionPresenterContract providesCreateTransactionPresenter(
            CreateTransactionInteractorContract createTransactionInteractor) {
        return new CreateTransactionPresenter(createTransactionInteractor);
    }

}
