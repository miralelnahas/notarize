package com.example.notarize.model.interactor;

import com.example.notarize.model.Entity.transaction.Document;
import com.example.notarize.model.Entity.transaction.UpdateDocumentData;

import io.reactivex.Observable;

public class UpdateDocumentInteractor extends UpdateDocumentInteractorContract {
    @Override
    public Observable<Document> updateDocument(String id, UpdateDocumentData updateDocumentData) {
        return super.updateDocument(id, updateDocumentData);
    }
}
