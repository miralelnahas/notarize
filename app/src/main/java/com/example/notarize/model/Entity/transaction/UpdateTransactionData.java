package com.example.notarize.model.Entity.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateTransactionData {

    @SerializedName("signer")
    @Expose
    Signer signer;

    public UpdateTransactionData(Signer signer) {
        this.signer = signer;
    }

    public Signer getSigner() {
        return signer;
    }

    public void setSigner(Signer signer) {
        this.signer = signer;
    }
}
