package com.example.notarize.model.api;

import com.example.notarize.model.Entity.transaction.AddDocumentData;
import com.example.notarize.model.Entity.transaction.DeleteDocumentResponse;
import com.example.notarize.model.Entity.transaction.DeleteTransactionResponse;
import com.example.notarize.model.Entity.transaction.Document;
import com.example.notarize.model.Entity.transaction.TransactionData;
import com.example.notarize.model.Entity.transaction.TransactionResponse;
import com.example.notarize.model.Entity.transaction.Transactions;
import com.example.notarize.model.Entity.transaction.UpdateDocumentData;
import com.example.notarize.model.Entity.transaction.UpdateTransactionData;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface NotarizeService {
    @Headers("apikey: KYh9ozQvNYfswyzDZPhqsLFp")
    @POST("transactions")
    Observable<TransactionResponse> addTransaction(@Body TransactionData data);

    @Headers("apikey: KYh9ozQvNYfswyzDZPhqsLFp")
    @GET("transactions/{id}")
    Observable<TransactionResponse> getTransaction(@Path("id") String id);

    @Headers("apikey: KYh9ozQvNYfswyzDZPhqsLFp")
    @DELETE("transactions/{id}")
    Observable<DeleteTransactionResponse> deleteTransaction(@Path("id") String id);

    @Headers("apikey: KYh9ozQvNYfswyzDZPhqsLFp")
    @PUT("transactions/{id}")
    Observable<TransactionResponse> updateTransaction(@Path("id") String id,
                                                      @Body UpdateTransactionData updateTransactionData);

    @Headers("apikey: KYh9ozQvNYfswyzDZPhqsLFp")
    @POST("transactions/{id}/documents")
    Observable<Document> addDocument(@Path("id") String id, @Body AddDocumentData addDocumentData);

    @Headers("apikey: KYh9ozQvNYfswyzDZPhqsLFp")
    @POST("transactions/{id}/notarization_ready")
    Observable<TransactionData> activateTransaction(@Path("id") String id);

    @Headers("apikey: KYh9ozQvNYfswyzDZPhqsLFp")
    @DELETE("documents/{id}")
    Observable<DeleteDocumentResponse> deleteDocument(@Path("id") String id);

    @Headers("apikey: KYh9ozQvNYfswyzDZPhqsLFp")
    @PUT("documents/{id}")
    Observable<Document> updateDocument(@Path("id") String id, @Body UpdateDocumentData updateDocumentData);

    @Headers("apikey: KYh9ozQvNYfswyzDZPhqsLFp")
    @GET("transactions")
    Observable<Transactions> listTransactions();

    @Headers("apikey: KYh9ozQvNYfswyzDZPhqsLFp")
    @POST("transactions/{id}/send_email")
    Observable<TransactionResponse> resendEmail(@Path("id")String id);
}
