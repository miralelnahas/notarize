package com.example.notarize.model.network;

import com.example.notarize.NotarizeApplication;

import javax.inject.Inject;

import retrofit2.Retrofit;

public final class NetworkClient {

    @Inject
    Retrofit retrofit;

    private static NetworkClient instance;

    private NetworkClient() {
        NotarizeApplication.getApplicationComponent().inject(this);
    }

    public static NetworkClient getNetworkInstance() {
        if (instance == null) {
            instance = new NetworkClient();
            return instance;
        }
        return instance;
    }

    public Retrofit getRetrofitClient() {
        return retrofit;
    }
}
