package com.example.notarize.model.inject.component;

import com.example.notarize.model.inject.module.TransactionModule;
import com.example.notarize.view.activities.showTransaction.ShowTransactionActivity;

import dagger.Component;

@CustomScopeName
@Component(dependencies = ApplicationComponent.class, modules = TransactionModule.class)
public interface TransactionComponent {
    void inject(ShowTransactionActivity showTransactionActivity);
}
