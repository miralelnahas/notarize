package com.example.notarize.model.interactor;

import com.example.notarize.model.Entity.transaction.DeleteTransactionResponse;
import com.example.notarize.model.Entity.transaction.TransactionData;
import com.example.notarize.model.Entity.transaction.TransactionResponse;
import com.example.notarize.model.Entity.transaction.Transactions;

import io.reactivex.Observable;

public class ListTransactionsInteractor extends ListTransactionsInteractorContract {
    @Override
    public Observable<Transactions> listTransactions() {
        return super.listTransactions();
    }

    @Override
    public Observable<DeleteTransactionResponse> deleteTransaction(String id) {
        return super.deleteTransaction(id);
    }

    @Override
    public Observable<TransactionResponse> resendEmail(String id) {
        return super.resendEmail(id);
    }

    @Override
    public Observable<TransactionData> activateTransaction(String id) {
        return super.activateTransaction(id);
    }
}
