package com.example.notarize.model.Entity.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionData {
    @SerializedName("document")
    @Expose
    private String document;

    @SerializedName("signer")
    @Expose
    private Signer signer;

    @SerializedName("draft")
    @Expose
    private String draft;

    public TransactionData(String document, Signer signer, String draft) {
        this.document = document;
        this.signer = signer;
        this.draft = draft;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public Signer getSigner() {
        return signer;
    }

    public void setSigner(Signer signer) {
        this.signer = signer;
    }

    public String getDraft() {
        return draft;
    }

    public void setDraft(String draft) {
        this.draft = draft;
    }
}
