package com.example.notarize.model.inject.component;

import javax.inject.Scope;

@Scope
public @interface CustomScopeName {
}
