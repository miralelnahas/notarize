package com.example.notarize.model.Entity.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteDocumentResponse {
    @SerializedName("message")
    @Expose
    String message;

    public String getMessage() {
        return message;
    }
}
