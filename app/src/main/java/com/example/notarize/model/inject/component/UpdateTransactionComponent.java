package com.example.notarize.model.inject.component;

import com.example.notarize.model.inject.module.UpdateTransactionModule;
import com.example.notarize.view.activities.UpdateTransactionActivity;

import dagger.Component;

@CustomScopeName
@Component(dependencies = ApplicationComponent.class, modules = UpdateTransactionModule.class)
public interface UpdateTransactionComponent {
    void inject(UpdateTransactionActivity updateTransactionActivity);
}
