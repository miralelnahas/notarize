package com.example.notarize.model.inject.component;

import com.example.notarize.NotarizeApplication;
import com.example.notarize.model.inject.module.InteractorModule;
import com.example.notarize.model.inject.module.RetrofitModule;
import com.example.notarize.model.interactor.CreateTransactionInteractorContract;
import com.example.notarize.model.interactor.ListTransactionsInteractorContract;
import com.example.notarize.model.interactor.TransactionInteractorContract;
import com.example.notarize.model.interactor.UpdateDocumentInteractorContract;
import com.example.notarize.model.interactor.UpdateTransactionInteractorContract;
import com.example.notarize.model.network.NetworkClient;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component (modules = {InteractorModule.class, RetrofitModule.class})
public interface ApplicationComponent {
    CreateTransactionInteractorContract getCreateTransactionInteractor();
    UpdateTransactionInteractorContract getUpdateTransactionInteractor();
    TransactionInteractorContract getTransactionInteractor();
    UpdateDocumentInteractorContract getUpdateDocumentInteractor();
    ListTransactionsInteractorContract getListTransactionsInteractor();

    Retrofit getRetrofitClient();

    void inject(NotarizeApplication notarizeApplication);
    void inject(NetworkClient networkClient);
}
