package com.example.notarize.model.Entity.transaction;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionResponse implements Parcelable {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("transaction_name")
    @Expose
    private String transactionName;

    @SerializedName("documents")
    @Expose
    private Document[] documents;

    @SerializedName("signers")
    @Expose
    private Signer[] signers;

    @SerializedName("status")
    @Expose
    private String status;

    protected TransactionResponse(Parcel in) {
        id = in.readString();
        transactionName = in.readString();
        documents = in.createTypedArray(Document.CREATOR);
        signers = in.createTypedArray(Signer.CREATOR);
        status = in.readString();
    }

    public static final Creator<TransactionResponse> CREATOR = new Creator<TransactionResponse>() {
        @Override
        public TransactionResponse createFromParcel(Parcel in) {
            return new TransactionResponse(in);
        }

        @Override
        public TransactionResponse[] newArray(int size) {
            return new TransactionResponse[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public Document[] getDocuments() {
        return documents;
    }

    public Signer[] getSigners() {
        return signers;
    }

    public String getStatus() {
        return status;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(transactionName);
        parcel.writeTypedArray(documents, i);
        parcel.writeTypedArray(signers, i);
        parcel.writeString(status);
    }
}
