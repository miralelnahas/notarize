package com.example.notarize.model.interactor;

import com.example.notarize.model.Entity.transaction.TransactionData;
import com.example.notarize.model.Entity.transaction.TransactionResponse;

import io.reactivex.Observable;

public class CreateTransactionInteractor extends CreateTransactionInteractorContract {
    @Override
    public Observable<TransactionResponse> addTransaction(TransactionData data) {
        return super.addTransaction(data);
    }
}
