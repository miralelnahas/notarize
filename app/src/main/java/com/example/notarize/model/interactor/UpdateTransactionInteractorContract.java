package com.example.notarize.model.interactor;

import com.example.notarize.model.Entity.transaction.TransactionResponse;
import com.example.notarize.model.Entity.transaction.UpdateTransactionData;

import io.reactivex.Observable;

public abstract class UpdateTransactionInteractorContract extends BaseInteractor {
    @Override
    public Observable<TransactionResponse> updateTransaction(String id, UpdateTransactionData updateTransactionData) {
        return super.updateTransaction(id, updateTransactionData);
    }
}
