package com.example.notarize.model.Entity.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddDocumentData {
    @SerializedName("document")
    @Expose
    String documentUrl;

    public AddDocumentData(String documentUrl) {
        this.documentUrl = documentUrl;
    }

    public String getDocumentUrl() {
        return documentUrl;
    }

    public void setDocumentUrl(String documentUrl) {
        this.documentUrl = documentUrl;
    }
}
