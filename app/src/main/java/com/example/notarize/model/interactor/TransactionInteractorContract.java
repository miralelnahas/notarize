package com.example.notarize.model.interactor;

import com.example.notarize.model.Entity.transaction.AddDocumentData;
import com.example.notarize.model.Entity.transaction.DeleteDocumentResponse;
import com.example.notarize.model.Entity.transaction.DeleteTransactionResponse;
import com.example.notarize.model.Entity.transaction.Document;
import com.example.notarize.model.Entity.transaction.TransactionResponse;

import io.reactivex.Observable;

public abstract class TransactionInteractorContract extends BaseInteractor {
    @Override
    public Observable<TransactionResponse> getTransaction(String id) {
        return super.getTransaction(id);
    }

    @Override
    public Observable<DeleteTransactionResponse> deleteTransaction(String id) {
        return super.deleteTransaction(id);
    }

    @Override
    public Observable<DeleteDocumentResponse> deleteDocument(String id) {
        return super.deleteDocument(id);
    }

    @Override
    public Observable<Document> addDocument(String id, AddDocumentData addDocumentData) {
        return super.addDocument(id, addDocumentData);
    }
}
