package com.example.notarize.model.inject.component;

import com.example.notarize.model.inject.module.UpdateDocumentModule;
import com.example.notarize.view.activities.UpdateDocumentActivity;

import dagger.Component;

@CustomScopeName
@Component(dependencies = ApplicationComponent.class, modules = UpdateDocumentModule.class)
public interface UpdateDocumentComponent {
    void inject(UpdateDocumentActivity updateDocumentActivity);
}
