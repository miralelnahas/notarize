package com.example.notarize.model.inject.component;

import com.example.notarize.model.inject.module.ListTransactionsModule;
import com.example.notarize.view.activities.listTransactions.ListTransactionsActivity;

import dagger.Component;

@CustomScopeName
@Component(dependencies = ApplicationComponent.class, modules = ListTransactionsModule.class)
public interface ListTransactionsComponent {
    void inject(ListTransactionsActivity listTransactionsActivity);
}
