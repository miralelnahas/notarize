package com.example.notarize.model.inject.component;

import com.example.notarize.model.inject.module.CreateTransactionModule;
import com.example.notarize.view.activities.CreateTransactionActivity;

import dagger.Component;

@CustomScopeName
@Component(dependencies = ApplicationComponent.class, modules = CreateTransactionModule.class)
public interface CreateTransactionComponent {
    void inject(CreateTransactionActivity createTransactionActivity);
}
