package com.example.notarize.model.inject;

public interface HasComponent<C> {
    C getComponent();
}
