package com.example.notarize.presenter;

import com.example.notarize.model.Entity.transaction.UpdateTransactionData;
import com.example.notarize.model.interactor.UpdateTransactionInteractorContract;
import com.example.notarize.view.UpdateTransactionView;

public abstract class UpdateTransactionPresenterContract
        extends BasePresenter<UpdateTransactionView, UpdateTransactionInteractorContract> {
    UpdateTransactionPresenterContract(UpdateTransactionInteractorContract interactor) {
        super(interactor);
    }

    public abstract void updateTransaction(String id, UpdateTransactionData updateTransactionData);
}
