package com.example.notarize.presenter;

import com.example.notarize.model.interactor.ListTransactionsInteractorContract;
import com.example.notarize.view.ListTransactionsView;

public abstract class ListTransactionsPresenterContract
        extends BasePresenter<ListTransactionsView, ListTransactionsInteractorContract> {
    ListTransactionsPresenterContract(ListTransactionsInteractorContract interactor) {
        super(interactor);
    }
    public abstract void listTransactions();
    public abstract void activateTransaction(String id);
    public abstract void deleteTransaction(String id);
    public abstract void resendEmail(String id);
}
