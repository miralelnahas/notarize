package com.example.notarize.presenter;

import com.example.notarize.model.Entity.transaction.UpdateDocumentData;
import com.example.notarize.model.interactor.UpdateDocumentInteractorContract;
import com.example.notarize.view.UpdateDocumentView;

public abstract class UpdateDocumentPresenterContract
        extends BasePresenter<UpdateDocumentView, UpdateDocumentInteractorContract> {
    UpdateDocumentPresenterContract(UpdateDocumentInteractorContract interactor) {
        super(interactor);
    }

    public abstract void updateDocument(String id, UpdateDocumentData updateDocumentData);
}
