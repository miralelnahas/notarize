package com.example.notarize.presenter;

import com.example.notarize.model.Entity.transaction.DeleteTransactionResponse;
import com.example.notarize.model.Entity.transaction.TransactionData;
import com.example.notarize.model.Entity.transaction.TransactionResponse;
import com.example.notarize.model.Entity.transaction.Transactions;
import com.example.notarize.model.interactor.ListTransactionsInteractorContract;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ListTransactionsPresenter extends ListTransactionsPresenterContract {
    public ListTransactionsPresenter(ListTransactionsInteractorContract interactor) {
        super(interactor);
    }

    public void listTransactions() {
        Observable<Transactions> listTransactionsObservable = getInteractor().listTransactions();
        listTransactionsObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new Observer<Transactions>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Transactions value) {
                        getView().showTransactions(value.getTransactions());
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void deleteTransaction(String id) {
        Observable<DeleteTransactionResponse> deleteTransactionObservable = getInteractor().deleteTransaction(id);
        deleteTransactionObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new Observer<DeleteTransactionResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addSubscriptions(d);
                    }

                    @Override
                    public void onNext(DeleteTransactionResponse value) {
                        getView().sendResponse(value.getMessage());
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().sendError("Something went wrong.");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void resendEmail(String id) {
        Observable<TransactionResponse> resendEmailObservable = getInteractor().resendEmail(id);
        resendEmailObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new Observer<TransactionResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(TransactionResponse value) {
                        getView().sendResendEmailResponse();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().sendError("Something went wrong.");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void activateTransaction(String id) {
        Observable<TransactionData> activateTransactionObservable = getInteractor().activateTransaction(id);
        activateTransactionObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new Observer<TransactionData>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addSubscriptions(d);
                    }

                    @Override
                    public void onNext(TransactionData value) {
                        getView().sendResponse("Transaction Activated Successfully");
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().sendError("Activation Failed! You need at least one document.");

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }
}
