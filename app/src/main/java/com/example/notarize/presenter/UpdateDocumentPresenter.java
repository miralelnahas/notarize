package com.example.notarize.presenter;

import android.util.Log;

import com.example.notarize.model.Entity.transaction.Document;
import com.example.notarize.model.Entity.transaction.UpdateDocumentData;
import com.example.notarize.model.interactor.UpdateDocumentInteractorContract;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class UpdateDocumentPresenter extends UpdateDocumentPresenterContract {
    public UpdateDocumentPresenter(UpdateDocumentInteractorContract interactor) {
        super(interactor);
    }

    @Override
    public void updateDocument(String id, UpdateDocumentData updateDocumentData) {
        Observable<Document> updateDocumentObservable = getInteractor().updateDocument(id, updateDocumentData);
        updateDocumentObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new Observer<Document>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addSubscriptions(d);
                    }

                    @Override
                    public void onNext(Document value) {
                        getView().sendUpdateDocumentResponse();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("document", "fail");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }
}
