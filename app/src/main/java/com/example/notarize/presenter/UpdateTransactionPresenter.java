package com.example.notarize.presenter;

import com.example.notarize.model.Entity.transaction.TransactionResponse;
import com.example.notarize.model.Entity.transaction.UpdateTransactionData;
import com.example.notarize.model.interactor.UpdateTransactionInteractorContract;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class UpdateTransactionPresenter extends UpdateTransactionPresenterContract {
    public UpdateTransactionPresenter(UpdateTransactionInteractorContract interactor) {
        super(interactor);
    }

    @Override
    public void updateTransaction(String id, UpdateTransactionData updateTransactionData) {
        Observable<TransactionResponse> updateTransactionObservable
                = getInteractor().updateTransaction(id, updateTransactionData);
        updateTransactionObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new Observer<TransactionResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addSubscriptions(d);
                    }

                    @Override
                    public void onNext(TransactionResponse transactionResponse) {
                        getView().sendUpdateTransactionResponse();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().sendUpdateTransactionError();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }
}
