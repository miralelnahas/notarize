package com.example.notarize.presenter;

import com.example.notarize.model.Entity.transaction.TransactionData;
import com.example.notarize.model.interactor.CreateTransactionInteractorContract;
import com.example.notarize.view.CreateTransactionView;

public abstract class CreateTransactionPresenterContract
        extends BasePresenter<CreateTransactionView, CreateTransactionInteractorContract> {

    CreateTransactionPresenterContract(CreateTransactionInteractorContract interactor) {
        super(interactor);
    }

    public abstract void addTransaction(TransactionData data);
}
