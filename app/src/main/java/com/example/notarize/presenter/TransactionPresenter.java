package com.example.notarize.presenter;

import android.util.Log;

import com.example.notarize.model.Entity.transaction.AddDocumentData;
import com.example.notarize.model.Entity.transaction.DeleteDocumentResponse;
import com.example.notarize.model.Entity.transaction.DeleteTransactionResponse;
import com.example.notarize.model.Entity.transaction.Document;
import com.example.notarize.model.Entity.transaction.TransactionData;
import com.example.notarize.model.Entity.transaction.TransactionResponse;
import com.example.notarize.model.interactor.TransactionInteractorContract;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class TransactionPresenter extends TransactionPresenterContract {
    public TransactionPresenter(TransactionInteractorContract interactor) {
        super(interactor);
    }

    @Override
    public void getTransaction(String id) {
        Observable<TransactionResponse> getTransactionObservable = getInteractor().getTransaction(id);
        getTransactionObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new Observer<TransactionResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addSubscriptions(d);
                    }

                    @Override
                    public void onNext(TransactionResponse transactionResponse) {
                        getView().sendGetTransactionResponse(transactionResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().sendGetTransactionError();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void deleteDocument(String id) {
        Observable<DeleteDocumentResponse> deleteDocumentObservable = getInteractor().deleteDocument(id);
        deleteDocumentObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new Observer<DeleteDocumentResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addSubscriptions(d);
                    }

                    @Override
                    public void onNext(DeleteDocumentResponse value) {
                        getView().sendDeleteDocumentResponse(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().sendError("Something went wrong.");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void addDocument(String id, AddDocumentData addDocumentData) {
        Observable<Document> addDocumentObservable = getInteractor().addDocument(id, addDocumentData);
        addDocumentObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new Observer<Document>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addSubscriptions(d);
                    }

                    @Override
                    public void onNext(Document value) {
                        getView().sendAddDocumentResponse();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().sendError("Something went wrong.");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void deleteTransaction(String id) {
        Observable<DeleteTransactionResponse> deleteTransactionObservable = getInteractor().deleteTransaction(id);
        deleteTransactionObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new Observer<DeleteTransactionResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addSubscriptions(d);
                    }

                    @Override
                    public void onNext(DeleteTransactionResponse value) {
                        getView().sendDeleteTransactionResponse(value.getMessage());
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().sendError("Something went wrong.");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void resendEmail(String id) {
        Observable<TransactionResponse> resendEmailObservable = getInteractor().resendEmail(id);
        resendEmailObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new Observer<TransactionResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(TransactionResponse value) {
                        getView().sendResendEmailResponse();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().sendError("Something went wrong.");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void activateTransaction(String id) {
        Observable<TransactionData> activateTransactionObservable = getInteractor().activateTransaction(id);
        activateTransactionObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new Observer<TransactionData>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addSubscriptions(d);
                    }

                    @Override
                    public void onNext(TransactionData value) {
                        getView().sendActivateTransactionResponse();
                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().sendError("Activation Failed! You need at least one document.");

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }
}
