package com.example.notarize.presenter;

import com.example.notarize.model.Entity.transaction.AddDocumentData;
import com.example.notarize.model.interactor.TransactionInteractorContract;
import com.example.notarize.view.TransactionView;

public abstract class TransactionPresenterContract
        extends BasePresenter<TransactionView, TransactionInteractorContract> {
    TransactionPresenterContract(TransactionInteractorContract interactor) {
        super(interactor);
    }
    public abstract void getTransaction(String id);
    public abstract void deleteDocument(String id);
    public abstract void addDocument(String id, AddDocumentData addDocumentData);
    public abstract void activateTransaction(String id);
    public abstract void deleteTransaction(String id);
    public abstract void resendEmail(String id);
}
