package com.example.notarize.presenter;

import android.util.Log;

import com.example.notarize.model.Entity.transaction.TransactionData;
import com.example.notarize.model.Entity.transaction.TransactionResponse;
import com.example.notarize.model.interactor.CreateTransactionInteractorContract;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CreateTransactionPresenter extends CreateTransactionPresenterContract {
    @Override
    public void addTransaction(TransactionData data) {
        Observable<TransactionResponse> addTransactionObservable = getInteractor().addTransaction(data);
        addTransactionObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new Observer<TransactionResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addSubscriptions(d);
                    }

                    @Override
                    public void onNext(TransactionResponse transactionResponse) {
                        getView().sendAddTransactionResponse(transactionResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("response", e.toString());
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    public CreateTransactionPresenter(CreateTransactionInteractorContract interactor) {
        super(interactor);
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }
}
