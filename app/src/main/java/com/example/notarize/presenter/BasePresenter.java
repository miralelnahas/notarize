package com.example.notarize.presenter;

import com.example.notarize.model.interactor.BaseInteractor;
import com.example.notarize.view.BaseView;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BasePresenter<V extends BaseView, I extends BaseInteractor> {


    I interactor;
    V view;
    CompositeDisposable subscriptions;

    BasePresenter(I interactor) {
        this.interactor = interactor;
    }
    public void attachView(V view) {
        this.view = view;
    }

    I getInteractor() {
        return interactor;
    }

    V getView() {
        return view;
    }

    void addSubscriptions(Disposable disposable) {
        if (subscriptions == null) {
            subscriptions = new CompositeDisposable();
        }
        subscriptions.add(disposable);
    }

    public abstract void onCreate();

    public abstract void onStart();

    public abstract void onResume();

    public abstract void onPause();

    public abstract void onStop();

    public abstract void onDestroy();


}
