package com.example.notarize;

import android.app.Application;

import com.example.notarize.model.inject.component.ApplicationComponent;
import com.example.notarize.model.inject.component.DaggerApplicationComponent;

public class NotarizeApplication extends Application {
    private static ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        injectApplicationComponent();
    }

    private void injectApplicationComponent() {
        applicationComponent = DaggerApplicationComponent.create();
        applicationComponent.inject(this);
    }

    public static ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
